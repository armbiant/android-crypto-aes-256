package ic.util.crypto.aes256.impl


import java.io.IOException

import ic.base.throwables.IoException

import ic.util.crypto.aes256.Aes256EncryptByteOutput


fun Aes256EncryptByteOutput.writePadding() {

	try {
		repeat(chunkLengthInBytes) {
			cipherOutputStream.write(0)
		}
	} catch (e: IOException) {
		throw IoException
	}

}