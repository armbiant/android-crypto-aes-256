package ic.util.crypto.aes256.impl


import javax.crypto.spec.IvParameterSpec

import ic.base.arrays.bytes.ByteArray


internal fun createInitVectorSpec() : IvParameterSpec {

	return IvParameterSpec(
		ByteArray(length = 128 / Byte.SIZE_BITS)
	)

}