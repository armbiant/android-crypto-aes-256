package ic.util.crypto.aes256.impl


import java.io.IOException

import ic.base.primitives.bytes.ext.asUByte
import ic.base.primitives.int32.ext.asByte
import ic.base.primitives.ubyte.ext.asInt32
import ic.base.throwables.End
import ic.base.throwables.End.End
import ic.base.throwables.IoException

import ic.util.crypto.aes256.Aes256DecryptByteInput


@Throws(IoException::class, End::class)
internal fun Aes256DecryptByteInput.readBytesLeft() {

	try {

		val i = cipherInputStream.read()
		if (i == -1) {
			cipherInputStream.close()
			isClosed = true
			End()
		}

		bytesLeftInChunk = i.asByte.asUByte.asInt32

	} catch (_: IOException) {
		throw IoException
	}

}