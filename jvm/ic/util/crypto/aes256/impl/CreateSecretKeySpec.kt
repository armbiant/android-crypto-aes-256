package ic.util.crypto.aes256.impl


import javax.crypto.spec.SecretKeySpec

import ic.util.crypto.aes256.Aes256Key


internal fun createSecretKeySpec (key: Aes256Key) : SecretKeySpec {

	return SecretKeySpec(key.asByteArray, "AES")

}