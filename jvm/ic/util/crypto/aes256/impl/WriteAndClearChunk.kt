package ic.util.crypto.aes256.impl


import ic.base.arrays.ext.clear
import ic.base.primitives.int32.ext.asByte
import ic.base.throwables.IoException

import ic.util.crypto.aes256.Aes256EncryptByteOutput


internal fun Aes256EncryptByteOutput.writeAndClearChunk() {

	try {

		chunk[0] = chunkBytesCount.asByte

		cipherOutputStream.write(chunk)

		chunk.clear()

		chunkBytesCount = 0

	} catch (_: java.io.IOException) {
		throw IoException
	}

}